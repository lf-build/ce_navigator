'use strict';

module.exports = function (grunt) {
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin',
        ngtemplates: 'grunt-angular-templates',
        cdnify: 'grunt-google-cdn',
        replace: 'grunt-text-replace',
        vulcanize: 'grunt-vulcanize',
        uglify: 'grunt-contrib-uglify-es',
        'sw-precache': 'grunt-sw-precache'
    });
    require('time-grunt')(grunt);

    var portal = grunt.option('portal') || 'LMS';

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        yeoman: {
            app: 'app',
            dist: 'dist'
        },
        clean: {
            options: {
                force: true
            },
            dist: {
                files: [
                    {
                        dot: true,
                        src: [
                            '.tmp',
                            '<%= yeoman.dist %>/{,*/}*',
                            '!<%= yeoman.dist %>/.git{,*/}*',
                            '!<%= yeoman.dist %>/node_modules'
                        ]
                    }
                ]
            },
            dev: {
                files: [
                    {
                        dot: true,
                        src: [
                            '.tmp',
                            '<%= yeoman.app %>/sw.js',
                            '<%= yeoman.app %>/styles/inspinia.css',
                            '<%= yeoman.app %>/alloy-core.html',
                            '<%= yeoman.app %>/vendor.html'
                        ]
                    }
                ]
            }
        },
        autoprefixer: {
            options: {
                browsers: ['last 1 version']
            },
            dev: {
                options: {
                    map: true,
                },
                files: [
                    {
                        expand: true,
                        cwd: '.tmp/styles/',
                        src: '{,*/}*.css',
                        dest: '.tmp/styles/'
                    }
                ]
            },
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: '.tmp/styles/',
                        src: '{,*/}*.css',
                        dest: '.tmp/styles/'
                    }
                ]
            }
        },
        wiredep: {
            app: {
                src: ['<%= yeoman.app %>/{,*/}*.html'],
                ignorePath: /\.\.\//,
                exclude: [
                    'bower_components/datatables/media/css/jquery.dataTables.css',
                    'polymer.html',
                    'polymer-mini.html',
                    'polymer-micro.html'
                ],
                options: {
                    fileTypes: {
                        html: {
                            detect: {
                                js: /<script.*src=['"]([^'"]+)/gi,
                                css: /<link.*href=['"]([^'"]+)/gi,
                                html: /<link.*href=['"]([^'"]+)/gi
                            },
                            replace: {
                                js: '<script src="{{filePath}}"></script>',
                                css: '<link rel="stylesheet" href="{{filePath}}" />',
                                html: '<link rel="import" href="{{filePath}}" />'
                            }
                        }
                    }
                }
            }
        },
        filerev: {
            dist: {
                src: [
                    '<%= yeoman.dist %>/scripts/{,*/}*.js',
                    '<%= yeoman.dist %>/styles/{,*/}*.css',
                    '<%= yeoman.dist %>/styles/fonts/*'
                ]
            }
        },
        useminPrepare: {
            html: '<%= yeoman.app %>/index.html',
            options: {
                dest: '<%= yeoman.dist %>',
                flow: {
                    html: {
                        steps: {
                            js: ['concat', 'uglifyjs'],
                            css: ['cssmin']
                        },
                        post: {}
                    }
                }
            }
        },
        usemin: {
            html: ['<%= yeoman.dist %>/**/*.html'],
            css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
            js: ['<%= yeoman.dist %>/scripts/{,*/}*.js'],
            options: {
                assetsDirs: [
                    '<%= yeoman.dist %>',
                    '<%= yeoman.dist %>/images',
                    '<%= yeoman.dist %>/styles'
                ],
                patterns: {
                    js: [[/(images\/[^''""]*\.(png|jpg|jpeg|gif|webp|svg))/g, 'Replacing references to images']]
                }
            }
        },
        imagemin: {
            dist: {
                option: {
                    optimizationLevel: 4
                },
                files: [
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>/images',
                        src: '{,*/}*.{png,jpg,jpeg,gif}',
                        dest: '<%= yeoman.dist %>/images'
                    }
                ]
            }
        },
        svgmin: {
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>/images',
                        src: '{,*/}*.svg',
                        dest: '<%= yeoman.dist %>/images'
                    }
                ]
            }
        },
        cssmin: {
            options: {
                mergeIntoShorthands: true
            }
        },
        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true,
                    conservativeCollapse: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true,
                    minifiyCSS: true,
                    minifiyJS: true
                },
                files: [
                    {
                        expand: true,
                        cwd: '<%= yeoman.dist %>',
                        src: ['*.html'],
                        dest: '<%= yeoman.dist %>'
                    }
                ]
            }
        },
        cdnify: {
            dist: {
                html: ['<%= yeoman.dist %>/**/*.html', '!<%= yeoman.dist %>/bower_components']
            }
        },
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= yeoman.app %>',
                        dest: '<%= yeoman.dist %>',
                        src: [
                            '*.{ico,png,txt}',
                            '.htaccess',
                            '*.html',
                            'images/{,*/}*.{webp}',
                            'styles/fonts/{,*/}*.*',
                            'components/**/*.*',
                            'favicons/**/*.*',
                            'config.js'
                        ]
                    },
                    {
                        expand: true,
                        cwd: '.tmp/images',
                        dest: '<%= yeoman.dist %>/images',
                        src: ['generated/*']
                    },
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>/bower_components/bootstrap/dist',
                        src: 'fonts/*',
                        dest: '<%= yeoman.dist %>'
                    },
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>/bower_components/fontawesome',
                        src: 'fonts/*',
                        dest: '<%= yeoman.dist %>'
                    },
                    {
                        expand: true,
                        cwd: 'bower_components/polymer',
                        dest: '<%= yeoman.dist %>/components/polymer',
                        src: ['*.html']
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= yeoman.app %>/bower_components/summernote/dist/font/',
                        src: 'fonts/*',
                        dest: '<%= yeoman.dist %>'
                    },
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>/bower_components',
                        src: "**/*.*",
                        dest: '<%= yeoman.dist %>/bower_components'
                    }
                ]
            },
            styles: {
                expand: true,
                cwd: '.tmp',
                dest: '<%= yeoman.app %>',
                src: [
                    '{,*/}*.css'
                ]
            }
        },
        concurrent: {
            dist: [
                'copy:styles',
                'imagemin',
                'svgmin'
            ],
            dev: [
                'copy:styles'
            ]
        },
        recess: {
            options: {
                compile: true,
                compress: true
            },
            dev: {
                files: {
                    '.tmp/styles/inspinia.css': '<%= yeoman.app %>/styles/inspinia/style.less'
                }
            },
            dist: {
                files: {
                    'dist/styles/inspinia.css': '<%= yeoman.app %>/styles/inspinia/style.less'
                }
            }
        },
        vulcanize: {
            dist: {
                options: {
                    inlineScripts: true
                },
                files: {
                    "dist/vendor.html": "dist/alloy.html",
                    "dist/alloy-core.html": "<%= yeoman.dist %>/**/core/platform-shell.html"
                }
            },
            dev: {
                options: {
                    inlineScripts: true
                },
                files: {
                    "app/vendor.html": "<%= yeoman.app %>/alloy.html",
                    "app/alloy-core.html": "<%= yeoman.app %>/**/core/platform-shell.html"
                }
            }
        },
        'sw-precache': {
            options: {
                cacheId: 'navigator',
                workerFileName: 'sw.js',
                verbose: true
            },
            dist: {
                staticFileGlobs: [
                    'styles/**/*.css',
                    'fonts/**/*.{woff,ttf,svg,eot}',
                    'images/**/*.{png,jpg,jpeg,gif}',
                    'scripts/**/*.js',
                    'components/**/*.*',
                    'vendor.html',
                    'alloy-core.html'
                ]
            },
            dev: {
                options: {
                    cacheId: 'navigator',
                    workerFileName: 'sw.js',
                    verbose: true,
                    baseDir: 'app'
                },
                staticFileGlobs: [
                    'styles/**/*.css',
                    'fonts/**/*.{woff,ttf,svg,eot}',
                    'images/**/*.{png,jpg,jpeg,gif}',
                    'scripts/**/*.js',
                    'components/**/*.*',
                    'vendor.html',
                    'alloy-core.html'
                ]
            }
        },
        replace: {
            dev: {
                src: ['<%= yeoman.app %>/alloy-core.html'],
                dest: '<%= yeoman.app %>/alloy-core.html',
                replacements: [
                    {
                        from: 'CONFIG_PORTAL',
                        to: function () {
                            return portal;
                        }
                    }
                ]
            }
        }
    });

    grunt.registerTask('develop', [
        'clean:dev',
        'wiredep',
        'recess:dev',
        'concurrent:dev',
        'autoprefixer:dev',
        'vulcanize:dev',
        'replace:dev',
        'sw-precache:dev'
    ]);

    grunt.registerTask('build', [
        'clean:dist',
        'wiredep',
        'recess:dist',
        'useminPrepare',
        'concurrent:dist',
        'autoprefixer',
        'concat',
        'copy:dist',
        'cssmin',
        'uglify',
        'filerev:dist',
        'usemin',
        'htmlmin:dist'
    ]);

    grunt.registerTask('service-worker', [
        'vulcanize:dist',
        'sw-precache:dist'
    ]);
};
