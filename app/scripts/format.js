const TICK = {
    TICKS_IN_MILLISECONDS: 10000,
    MAX_DATE_MILLISECONDS: 8640000000000000,
    EPOCH_TICKS: 621355968000000000,
    MAX_TICKS: 3155378975999999999,
    MIN_TICKS: 0,
    NET_START_DATE: new Date("0001-01-01").getTime()
};

const EXCLUDE_DATES = ["01-01-0001", "01/01/0001", "0001-01-01T00:00:00Z", "0001-01-01T00:00:00+00:00"];

const COUNTRY = {
    USA: {
        DATE_FORMAT: {
            DATE: 'MM/DD/YYYY',
            DATE_TIME: 'MM/DD/YYYY hh:mm:ss a'
        },
        ICON: 'fa fa-usd',
        LOCALE: 'en-US',
        CURRENCY: { style: 'currency', currency: 'USD' },
        MASK_FORMAT: {
            SSN: '*****XXXX',
            PHONE: '(XXX) XXX-XXXX',
            ACCOUNT_NUMBER: '*************XXXX',
            TAX_ID: 'XX-XXXXXXX'
        }
    },
    INDIA: {
        DATE_FORMAT: {
            DATE: 'DD-MM-YYYY',
            DATE_TIME: 'DD-MM-YYYY LT'
        },
        ICON: 'fa fa-inr',
        LOCALE: 'en-In',
        CURRENCY: { style: 'currency', currency: 'INR' },
        MASK_FORMAT: {
            PHONE: 'XXXXX-XXXXX'
        }
    }
};

const formats = {
    _validateInput: function (input) {
        if (input === null || input === undefined || input === '') {
            return false;
        }
        return true;
    },
    _validateCountry: function (input) {
        if (!this._validateInput(input)) {
            return false;
        }

        if (!COUNTRY.hasOwnProperty(input)) {
            return false;
        }

        return true;
    },
    currencySymbol: function (country) {
        if (!this._validateCountry(country)) {
            return "";
        }
        var countryObj = COUNTRY[country];
        return countryObj.ICON;
    },
    date: function (input, country, withTime) {
        if (!this._validateInput(input) || !this._validateCountry(country)) {
            return "NA";
        }
        var countryObj = COUNTRY[country];
        if (withTime) {
            return moment.tz(input, window.config.timezone).format(countryObj.DATE_FORMAT.DATE_TIME);
        } else {
            //As time is not there it should not use timezone
            return moment(input).format(countryObj.DATE_FORMAT.DATE);
        }
    },
    onlyDate: function (input, country) {
        if (!this._validateInput(input) || !this._validateCountry(country) || EXCLUDE_DATES.indexOf(input.toString()) !== -1) {
            return "NA";
        }
        var dateList = input.split("T"); //Getting only date part from the input
        return this.date(dateList[0], country, false);
    },
    netTicksToDate: function (ticks, timezoneOffsetHours) {
        if (isNaN(ticks)) {
            return "Invalid Date";
        }
        var millisecondsSinceEpoch = (ticks - TICK.EPOCH_TICKS) / TICK.TICKS_IN_MILLISECONDS;
        if (millisecondsSinceEpoch > TICK.MAX_DATE_MILLISECONDS) {
            return "Invalid Date";
        }
        var date = new Date(millisecondsSinceEpoch);
        return date.toISOString();
    },
    money: function (input, country, fractionDigits) {
        if (!this._validateInput(input) || !this._validateCountry(country)) {
            return input;
        }
        var countryObj = _.cloneDeep(COUNTRY[country]);
        var amount = parseFloat(input);
        return amount.toLocaleString(countryObj.LOCALE, countryObj.CURRENCY);
    },
    moneyWithoutFraction: function (input, country, fractionDigits) {
        if (!this._validateInput(input) || !this._validateCountry(country)) {
            return input;
        }
        var countryObj = _.cloneDeep(COUNTRY[country]);
        if (fractionDigits === 0) {
            countryObj.CURRENCY.minimumFractionDigits = 0;
        }
        else {
            countryObj.CURRENCY.minimumFractionDigits = fractionDigits || 2;
        }
        var amount = parseFloat(input);
        return amount.toLocaleString(countryObj.LOCALE, countryObj.CURRENCY);
    },
    _maskedString: function (input, mask) {
        var output = '', s = '' + input;
        for (var im = 0, is = 0; im < mask.length && is < s.length; im++) {
            if (mask.charAt(im) === 'X') {
                output += s.charAt(is);
                is++;
            } else if (mask.charAt(im) === '*') {
                output += mask.charAt(im);
                is++;
            } else {
                output += mask.charAt(im);
            }
        }
        return output;
    },
    phone: function (input, country) {
        if (!this._validateInput(input) || !this._validateCountry(country)) {
            return input;
        }
        try {
            var countryObj = COUNTRY[country];
            return this._maskedString(input.replace(/[\s.()-]/g, ""), countryObj.MASK_FORMAT.PHONE);
        } catch (e) {
            console.error('Error converting provided input %s to Phone format', input);
            return input;
        }
    },
    federalSalesTaxID: function (input, country) {
        if (!this._validateInput(input) || !this._validateCountry(country)) {
            return input;
        }
        var countryObj = COUNTRY[country];
        return this._maskedString(input, countryObj.MASK_FORMAT.TAX_ID);
    },
    ssn: function (input, country) {
        if (!this._validateInput(input) || !this._validateCountry(country)) {
            return input;
        }
        var countryObj = COUNTRY[country];
        return this._maskedString(input, countryObj.MASK_FORMAT.SSN);
    },
    accountNumber: function (input, country) {
        if (!this._validateInput(input) || !this._validateCountry(country)) {
            return input;
        }
        var countryObj = _.cloneDeep(COUNTRY[country]);
        var maskedFormat = countryObj.MASK_FORMAT.ACCOUNT_NUMBER;
        if (maskedFormat.length > input.length) {
            maskedFormat = maskedFormat.substring((maskedFormat.length - input.length), maskedFormat.length);
        }
        return this._maskedString(input, maskedFormat);
    },
    round: function (input, fractionDigits, defaultOutput) {
        if (this._validateInput(input)) {
            var digit = this._validateInput(fractionDigits) ? fractionDigits : 0;
            var output = parseFloat(input);
            return output.toFixed(digit);
        }
        return defaultOutput || 0.00;
    },
    taxid: function (input) {
        if (this._validateInput(input) && input.length === 9) {
            try {
                return input.replace(/(\d{2})(\d{3})/, "$1-$2");
            }
            catch (e) {
                console.error('Error converting provided input %s to taxid format', input);
                return input;
            }
        }
        return 'Not Available';
    },
    bool2value: function (input) {
        if (!this._validateInput(input)) {
            return 'Not Available';
        }
        return input === true ? 'Yes' : (input === false ? 'No' : 'Not Available');
    }
}