var scoring = {
    expToValue: function (strVal) {
        if (!isNaN(strVal)) {
            if (_.isNumber(_.toNumber(strVal))) {
                return strVal;
            } else {
                if (this.isDateString(strVal)) {
                    return true;
                }
            }
        }
        strVal = strVal.toLowerCase();
        if (strVal === "yes") {
            return true;
        } else if (strVal === "no") {
            return false;
        } else if (strVal === "match") {
            return "\'match\'";
        } else if (strVal === "no match") {
            return "\'no match\'";
        }
        else {
            return "Not Available";
        }
    },
    isDateString: function (dateStr) {
        return dateStr.match(/\d{2}(\/)\d{2}(\/)\d{4}$/g);
    },
    evalCondFormat: function (field) {
        var configValAndIconsClassNameMapping = {
            tick: "fa fa-check green",
            cross: "fa fa-times red",
            dash: "fa fa-minus bluedash",
            red: "red",
            on: "fa fa-toggle-on",
            off: "fa fa-toggle-off",
            que: "fa fa-question"
        };
        var fieldVal = field.value;
        if (_.isEmpty(field.value) && _.isNull(field.value) && _.isUndefined(field.value)) {
            return "";
        }
        if (!_.isNumber(field.value) && field.value.indexOf('$') === 0) {
            fieldVal = field.value.replace("$", "");
            if (!_.isNaN(parseInt(fieldVal))) {
                fieldVal = parseInt(fieldVal);
            }
        }
        field.condFormat = field.condFormat.replace("this", this.expToValue(fieldVal));
        if (field.condFormat.includes("Not Available")) {
            return configValAndIconsClassNameMapping.que;
        }
        var evalData = eval(field.condFormat);
        return configValAndIconsClassNameMapping[evalData];
    },
    setDateVariables: function (field) {
        var dateStr = moment(field.value, 'MM/DD/YYYY');
        field.day = dateStr.format('DD');
        field.month = dateStr.format('MMM');
        field.year = dateStr.format('YYYY');
    },
    hasDateFormat: function (field) {
        if (field.format && field.format === "date") {
            if (!this.isDateString(field.value)) {
                return false;
            }
            this.setDateVariables(field);
            return true;
        }
        return false;
    },
    extractDataNameFromExpr: function (exp) {
        var operators = ["+", "/", "-", "*", "&&", "||"];
        var self = this;
        var operands = [];
        var operand = null;
        var length = operators.length;
        for (var i = 0; i < length; i++) {
            var op = operators[i];
            if (exp.includes(op)) {
                var result = exp.split(op);
                exp = result.pop();
                operand = result.pop(operand);
                operand = this.getOperand(operand);
                operands.push(operand);
            }
        }
        operand = this.getOperand(exp);
        operands.push(operand);
        return operands;
    },
    getOperand: function (exp) {
        var operand = null;
        operand = exp.replace(/this/g, '');
        operand = operand.replace(/[\(\)\[\]\']/g, '');
        return operand;
    },
    hasCondFormat: function (field) {
        if (field.condFormat) {
            return true;
        }
        return false;
    },
    showErrorMessage: function (event, message, hideAfter, hideOnevent) {
        var htmlString = '<div class="alert alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' + message + '</div>';
        event.target.insertAdjacentHTML('afterend', htmlString);
        if (hideAfter) {
            setTimeout(function () {
                scoring.hideErrorMessage(event);
            }, hideAfter);
        } else if (hideOnevent) {
            window.addEventListener(hideOnevent, function () { scoring.hideErrorMessage(event); }, false);
        }
    },
    hideErrorMessage: function (event) {
        event.target.nextElementSibling.remove();
    }
};

JSON.flatten = function (data) {
    var result = [];

    function recurse(cur, prop) {
        if (Object(cur) !== cur && cur !== null && cur !== '') {
            result.push(cur);
        } else if (Array.isArray(cur)) {
            for (var i = 0, l = cur.length; i < l; i++) {
                recurse(cur[i], prop ? prop + "." + i : "" + i);
            }
        } else {
            var isEmpty = true;
            for (var p in cur) {
                isEmpty = false;
                recurse(cur[p], prop ? prop + "." + p : p);
            }
        }
    }
    recurse(data, "");
    return result;
};

function showOverlay() {
    $("div.overlay").show();
    $("#paceContainer").show();
    $("#paceContainer .pace").removeClass('pace-inactive');
    $("#paceContainer .pace").show();
}

function hideOverlay() {
    $("div.overlay").hide();
    $("#paceContainer").hide();
    $("#paceContainer .pace").hide();
}