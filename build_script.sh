#bash build_script.sh TenantName ImageName TagName

docker build --build-arg tenant=$1 --no-cache -t registry.lendfoundry.com/$2:$3 .

docker push registry.lendfoundry.com/$2:$3