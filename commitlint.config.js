module.exports = {
    extends: ['@commitlint/config-conventional'],
    rules: {
        'body-leading-blank': () => [1, 'always'],
        'footer-leading-blank': () => [1, 'always'],
        'scope-case': () => [2, 'always', 'lower-case'],
        'subject-case': () => [2, 'never', ['sentence-case', 'start-case', 'pascal-case', 'upper-case']],
        'header-max-length': () => [0, "always", 72],
        'type-case': () => [2, 'always', 'lower-case'],
        'type-empty': () => [2, 'never'],
        'type-enum': () => [2, 'always', ['feat', 'breaking', 'test', 'chore', 'build', 'fix', 'bugs', 'style', 'refactor', 'test', 'revert']]
    }
};