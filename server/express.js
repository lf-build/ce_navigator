'use strict';

let path = require('path');
let cors = require('cors');
let express = require('express');
let helmet = require('helmet');
let bodyParser = require('body-parser');
let compression = require('compression');
let environment = process.env.NODE_ENV || "development";

function shouldCompress(req, res) {
    if (req.headers['x-no-compression']) {
        // don't compress responses with this request header
        return false;
    }
    // fallback to standard filter function
    return compression.filter(req, res);
}

module.exports = (app) => {
    let root = path.normalize(__dirname + '/..');

    app.use(helmet());
    app.set('views', root + '/server');
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html');

    app.use(cors());
    app.use(bodyParser.json({ limit: '2mb' }));
    app.use(bodyParser.text({ type: 'text/html', limit: '6mb' }));
    app.use(compression({ filter: shouldCompress }));

    if (environment === "production") {
        app.use(express.static(root + '/dist'));
        app.set('appPath', root + '/dist');
    }
    else {
        app.use(express.static(root + '/app'));
        app.set('appPath', root + '/app');
    }
}