const APP_TOKEN = process.env.APP_TOKEN || "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDEtMTFUMDU6MjI6MTEuNzg5MDQ3WiIsImV4cCI6IjIwMTctMDEtMTFUMDU6MzI6MTEuNzg3NTAxWiIsInN1YiI6ImNhdnAiLCJ0ZW5hbnQiOiJjYXBpdGFsLWFsbGlhbmNlIiwic2NvcGUiOlsiYXBpL3Zwb2ZzYWxlcyIsImFwaS9sb2FuY29vcmRpbmF0b3JtYW5hZ2VyIiwiYXBpL2xvYW5jb3JkaW5hdG9yIiwiYXBpL3NycmVsYXRpb25zaGlwbWFuYWdlciIsImFwaS9yZWxhdGlvbnNoaXBtYW5hZ2VyIl0sIklzVmFsaWQiOnRydWV9.ER1BnvTkFxlC0a1h4WiTStUCPEhNqb1p-DO2mqbYV5s";

function requestHeaderMiddleware(req, res, next) {
    if (!req.headers['authorization']) {
        req.headers['authorization'] = APP_TOKEN;
    }
    next();
}

module.exports = requestHeaderMiddleware;